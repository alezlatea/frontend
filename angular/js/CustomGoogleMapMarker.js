function CustomMarker(latlng, map, args) {
	this.latlng = latlng;	
	this.args = args;	
	this.setMap(map);	
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function() {
	
	var self = this;
	
	var div = this.div;
	
	if (!div) {
	
		div = this.div = document.createElement('div');
		
		div.className = 'marker';
		
		div.style.position = 'absolute';
		div.style.cursor = 'pointer';

		div.style.background = '#ff5a5f';
		div.style.padding = '2px 4px';
		div.innerHTML = "100";
		div.style.color = 'White';
		div.style.fontWeight = 'Bold';
		
		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}
		
		google.maps.event.addDomListener(div, "click", function(event) {
			google.maps.event.trigger(self, "click");
		});
		
		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}
	
	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
	
	if (point) {
		div.style.left = (point.x - 10) + 'px';
		div.style.top = (point.y - 20) + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}	
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng;	
};


function initialize() {

    var locations = [
  ['Bondi Beach', -33.890542, 151.274856, 4],
  ['Coogee Beach', -33.923036, 151.259052, 5],
  ['Cronulla Beach', -34.028249, 151.157507, 3],
  ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
  ['Maroubra Beach', -33.950198, 151.259302, 1]
  ['Maroubra Beach', -33.900542, 151.304856, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map-canvas'));
    myLatlng = new google.maps.LatLng(-33.890542, 151.274856);
    map.setZoom(10);
    map.setCenter(myLatlng);

    for (i = 0; i < locations.length; i++) {
        myLatlng = new google.maps.LatLng(locations[i][1], locations[i][2]);
        overlay = new CustomMarker(
        myLatlng,
        map,
        {
            marker_id: i
        }
    );

    }

}

$(document).ready(function(){initialize();});

