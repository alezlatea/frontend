app.controller("ProfileCtrl", function($scope, $http){
  
  // the array which represents the list
  $scope.loading = true;
  
  // this function fetches a random text and adds it to array
  $scope.more = function(){
    $http.get('data/profile.json').success(function(data){
      
		// returned data contains an array of 2 sentences
		$.each(data, function(k,val){
			newItem = val;
			$scope.items.push(val);
		});

		for(i=0;i<3;i++){
			$scope.items.shift();
		}
		
      	$scope.loading = false;
    });
  };
  
  // we call the function twice to populate the list
  $scope.more();
});

// we create a simple directive to modify behavior of <ul>
app.directive("whenScrolled", function(){
  return{
    
    restrict: 'A',
    link: function(scope, elem, attrs){
    
      // we get a list of elements of size 1 and need the first element
      raw = elem[0];
    
      // we load more elements when scrolled past a limit
      elem.bind("scroll", function(){
        if(raw.scrollTop+raw.offsetHeight+5 >= raw.scrollHeight){
          scope.loading = true;
          
        // we can give any function which loads more elements into the list
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});