app.controller('CategoryUploadCtrl', ['$scope', '$http', 'FileUploader', function ($scope, $http, FileUploader) {
	$scope.message = false;
    $scope.category = {};
    var images = [];
   
    //$scope.category = {};
    $http({
        method: 'GET',
        url: 'api/categorys'
    }).success(function (data) {
        $scope.categoryList = data;
       // console.log("got category data " + data)

    });
    
    

    
    //save category
    $scope.saveCategory = function () {
    	//process parent Category
    	if(angular.isUndefined($scope.category.parentCategory) == true){
    		var parentCat = null;
    	}else{
    		parentCat = {'id' : $scope.category.parentCategory};
    	}
    	

        var formData = {
            'imagess': images,
            'categoryName': $scope.category.categoryName,
            'categoryInfo': $scope.category.categoryInfo,
            'parentCategory' : parentCat
        }


        $http({
            method: 'POST',
            url: 'api/categorys',
            data: formData

        }).success(function (response) {
        	$scope.messageClass ="alert-success";
        	$scope.message = 'Category has been added.';
        	
        	//reset form data
        	images =[];
            $scope.category = {};
            $scope.uploader.queue = [];
            $scope.uploader.progress = 0;
        }).error(function(response){
        	$scope.messageClass ="alert-danger";
        	$scope.message = 'Network error'
        });
        
        
    }


    var uploader = $scope.uploader = new FileUploader({
        url: 'http://localhost:8080/api/upload/amenity'
    });


    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {

    };
    uploader.onAfterAddingFile = function (fileItem) {

    };
    uploader.onAfterAddingAll = function (addedFileItems) {

    };
    uploader.onBeforeUploadItem = function (item) {
        item.formData.push({"imagecategory":  "2"});

    };
    uploader.onProgressItem = function (fileItem, progress) {

    };
    uploader.onProgressAll = function (progress) {

    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {

    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {

    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {

    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        images.push({'id': response.id});
    };
    uploader.onCompleteAll = function () {


    };

}]);

