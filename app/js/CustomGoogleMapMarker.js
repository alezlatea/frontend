function CustomMarker(infoWindow, id, latlng, map, args) {
    this.latlng = latlng;
    this.args = args;
    this.setMap(map);
    this.markerId = id;
    this.infoWindow = infoWindow;
    this.map = map;
}

CustomMarker.prototype = new google.maps.OverlayView();

function isInfoWindowOpen(infoWindow) {
    var map = infoWindow.getMap();
    return (map !== null && typeof map !== "undefined");
}

CustomMarker.prototype.draw = function () {

    var self = this;

    var div = this.div;

    if (!div) {

        div = this.div = document.createElement('div');

        div.className = 'marker';

        div.style.position = 'absolute';
        div.style.cursor = 'pointer';

        div.style.background = '#ff5a5f';
        div.style.padding = '2px 4px';

        div.style.color = 'White';
        div.style.fontWeight = 'Bold';

        div.innerHTML = "$ " + self.markerId;
        div.id = self.markerId;

        //var content = '<div class="infoWindowContent" style="font-size:  14px !important; border-top: 1px solid #ccc; padding-top: 10px;">' + "Some Text " + self.markerId + '</div>';

        var content = '<div class="tooltip-map">' +
          '<div class="images">' +
              ' <div> ' +
                         '<img src = "img/images/img3.png">' +
                      '<div class="icon-heart"><i class="fa fa-heart-o icon-white"></i></div>' +
                      '<div class="icon-heart"><i class="fa fa-heart icon-black"></i></div>' +

              
              '<div class="panel-box">' +
                 ' <div class="price-box">' +
                     ' <span class="currency-sign">$</span>' +
                     ' <span class="price-airbnb">89</span>' +
                    '  <span class="currency">AUD</span>' +
                      '<span class="per-night">Per night</span>' +
                  '</div>' +
                  '<div class="text-under-image" style="top:0 !important">' +
                     ' <span ><a href="">Private room with own Tv and Netflix </a> </span><br>' +
                          '<span class="floatleft">Private room </span>' +
                          '<li class="floatleft">' +
                             ' <span class="glyphicon glyphicon-star"' +
                                                      'style="color: #F37826"></span>' +
                              '<span class="glyphicon glyphicon-star"' +
                                          ' style="color: #F37826"></span>' +
                              '<span class="glyphicon glyphicon-star"' +
                                           'style="color: #F37826"></span>' +
                             ' <span class="glyphicon glyphicon-star"' +
                                          ' style="color: #F37826"></span>' +
                             ' <span class="glyphicon glyphicon-star"' +
                                          ' style="color: #F37826"></span>' +
                         ' </li>' +
                         ' <li>4 review</li>' +
                    '  </div>' +
             ' </div>' +
             ' </div>' +
         ' </div>' +

      '</div>';
        localStorage.setItem("count", "0");
        localStorage.setItem("clicked", "0");
        google.maps.event.addListener(self, 'click', function (event) {
           // self.infoWindow.setContent(content);
            
            //self.infoWindow.open(self.map, self);
            localStorage.setItem("isMarker", 1);
            if (localStorage.getItem("count") == "0") {
                if (localStorage.getItem("clicked") == "1") {
                    self.infoWindow.close();
                    localStorage.setItem("clicked", "0");
                } else {
                    self.infoWindow.setContent(content);
                    localStorage.setItem("clicked", "1");
                    self.infoWindow.open(self.map, self);
                    
                }
            } else {
                console.log('intra iaic');
                self.infoWindow.close();
                localStorage.setItem("clicked", "0");
            }


        });
        google.maps.event.addListener(self.map, "click", function (event) {
            console.log('intra aici');
        });

        if (typeof (self.args.marker_id) !== 'undefined') {
            div.dataset.marker_id = self.args.marker_id;
        }

        google.maps.event.addDomListener(div, "click", function (event) {
            google.maps.event.trigger(self, "click");
        });

        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
    }

    var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

    if (point) {
        div.style.left = (point.x - 10) + 'px';
        div.style.top = (point.y - 20) + 'px';
    }
};

CustomMarker.prototype.remove = function () {
    if (this.div) {
        this.div.parentNode.removeChild(this.div);
        this.div = null;
    }
};

CustomMarker.prototype.getPosition = function () {
    return this.latlng;
};


function initialize() {

    var locations = [
  ['Coogee Beach', -33.923036, 151.259052, 5],
  ['Bondi Beach', -33.890542, 151.274856, 4],
  ['Cronulla Beach', -34.028249, 151.157507, 3],
  ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
  ['Maroubra Beach', -33.950198, 151.259302, 1],
  ['Maroubra Beach', -33.900542, 151.304856, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map-canvas'));
    myLatlng = new google.maps.LatLng(-33.890542, 151.274856);
    map.setZoom(10);
    map.setCenter(myLatlng);

    var infoWindow = new google.maps.InfoWindow({
        maxWidth: 250,
        width: 250,
        height: 300
        
     });

    for (i = 0; i < locations.length; i++) {
        myLatlng = new google.maps.LatLng(locations[i][1], locations[i][2]);
        overlay = new CustomMarker(
        infoWindow,
        i,
        myLatlng,
        map,
        {
            marker_id: i
        }

    );
    }
}

$(document).ready(function () {
    initialize();
});

