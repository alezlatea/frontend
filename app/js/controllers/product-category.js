app.controller('productCategoryCtrl', ['$scope', '$http','$sce', 'FileUploader',
  function ($scope, $http, $sce, FileUploader) {
   $scope.categoryViewStatus = true;
   $scope.product = {};
   var images = [];
    //save category
    $scope.submit = function () {
      //process parent Category
      

        var formData = {
            'imagess': images,
            'productName': $scope.product.productName
            //'categoryInfo': $scope.category.categoryInfo,
            //'parentCategory' : parentCat
        }


        $http({
            method: 'POST',
            url: 'api/products/add',
            data: formData

        }).success(function (response) {
          $scope.messageClass ="alert-success";
          $scope.message = 'Category has been added.';
          console.log('success');
          
          //reset form data
          images =[];
            $scope.category = {};
            $scope.uploader.queue = [];
            $scope.uploader.progress = 0;
        }).error(function(response){
          $scope.messageClass ="alert-danger";
          $scope.message = 'Network error';
          console.log('error');
        });
        
        
    }   
    $scope.category = [
    {
    	id : 1,
    	parent_id : null,
    	name : 'parent_cat_1',
    	child : [
                 {
                   	id : 4,
                   	parent_id : 1,
                   	name : 'child_cat_1',
                   	child : [
                             {
                                	id : 4,
                                	parent_id : 4,
                                	name : 'child_cat_1',
                                	
                            	} ,
                            	{
                                	id : 5,
                                	parent_id : 4,
                                	name : 'child_cat_2'
                            	},
                            	
                            	{
                                	id : 6,
                                	parent_id : 4,
                                	name : 'child_cat_3'
                            	}  
                                                  
                          ]
               	} ,
               	{
                   	id : 5,
                   	parent_id : 1,
                   	name : 'child_cat_2'
               	},
               	
               	{
                   	id : 6,
                   	parent_id : 1,
                   	name : 'child_cat_3'
               	}  
                                     
             ]
	} ,
	{
    	id : 2,
    	parent_id : null,
    	name : 'parent_cat_2'
	},
	
	{
    	id : 3,
    	parent_id : null,
    	name : 'parent_cat_3'
	}  
                      
  ];
   
   
   
   var getCategoryHtml = function(data){
	   
	   var html = '<ul>';
	   $.each(data, function(key,val){
		   html += '<li><a href="javascript:void(0)">'+val.name+'</a>';
		   if(angular.isUndefined(val.child) == false){
			   html += getCategoryHtml(val.child);
			   html += '</li>';
		   }else{
			   html += '</li>';
		   }
	   });
	   html += '</ul>';
	   
	   return html;
	   
   }
   
   $scope.printCategory = function(){
	  return $sce.trustAsHtml(getCategoryHtml($scope.category));
   }
   console.log($scope.printCategory());





    var uploader = $scope.uploader = new FileUploader({
        url: '/upload/uploadImage'
    });


    // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function (item , options) {
            return this.queue.length < 10;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function (item , filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        item.formData.push({"imagecategory":  "category"});
        item.formData.push({"catid":  "2"});
        console.log(item);
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        images.push({'id': response.id});
        console.log('push kro khush rho');
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');

    };
    console.log(uploader);
    console.info('uploader', uploader);

  
   
   
		   
   
}]);