/**
 * Created by Alexandra on 2/12/2016.
 */
'use strict';

app.controller("AirbnbCtrl", function($scope, $http){
    $('body').css('overflow','hidden');
    // hide or show filter button
    $scope.showFilters = function(visibility) {
        if (visibility == 'true') {
            $('#filters').removeClass('display-none');
			$('#btnMoreFilters').addClass('display-none');
        } else {
            $('#filters').addClass('display-none');
		    $('#btnMoreFilters').removeClass('display-none');

        }
    }

    $scope.myInterval = 5000;
    var slides = $scope.slides = [];
    var sliders = $scope.sliders = [];
    $scope.addSlide = function() {
        slides.push({
            image: 'img/img' + [slides.length % 3] + '.png',
            text: ['Carousels require the use of an id in the slide data caption',
                'Contrast and Similarity in Graphic Design are necessary to go',
                'Bacon ipsum dolor sit amet nulla dolor sit amet nulla',
                'Responsive treatment of angular apps'][slides.length % 4],
            smallImage: 'img/imgSmall0.png'
        });
    };
   // caroulse
    $scope.addSlider = function() {
        sliders.push({
            image: 'img/img' + [slides.length % 3] + '.png'
        });
    };

    for (var i=0; i<3; i++) {
        $scope.addSlide();
    }

    $http.get('data/airbnb.json').then(function(res){

        $scope.airbnbSliders = res.data;
    }); 
	$http.get('data/filters.json').then(function(res){

        $scope.filters = res.data;
		console.log($scope.filters);
    });
    
});