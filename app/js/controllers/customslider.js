
app.controller("CustomSliderCtrl", function($scope,$http) {
    $scope.formData = {};
	$scope.btnClass = "active";
    $scope.sliderConfig = {
        min: 10,
        max: 280
    }
    $scope.formData.flexibility = 'Flexible';
   
    $scope.formData.journeyType = '0';
    $scope.formData.luggageSpace = '0';
    $scope.formData.vehicleType = 'car';
    $scope.formData.journeyPrice = 10;  
	
    $scope.setPrice = function(journeyPrice) {
   	 $scope.formData.journeyPrice = journeyPrice;    
    }
	$scope.formData.journeyDate = new Date();   
	
	/* Calendar*/
	  
	$scope.formData.seatsAvailable = 1;   

	$http.get('http://www.wikibackpacker.com/wikibackend/api/getRideCitys?max=100')
	//$http.get('http://localhost/angular/frontend/app/partials/country.html')
	.success(function(data, status, headers, config) {
		
		$scope.departures  = data;
	})
	.error(function(error, status, headers, config) {
		 console.log(status);
		 console.log("Error occured");
	});	

	$scope.formData.fromLocation = '';
	$scope.formData.toLocation = '';
	$scope.setCountryName = function() {
	  // From Location Model
	  $scope.formData.fromLocation = $scope.formData.fromLocation;
	  $scope.formData.fromLocationDisplayName = $("#selectFrom option:selected").html();
	  // To Location Model
	  $scope.formData.toLocation = $scope.formData.toLocation;
	  $scope.formData.toLocationDisplayName = $("#selectTo option:selected").html();
	  
	};



	//alert($scope.fromLocation);
	$scope.processForm = function() {
		var description      = $scope.formData.description;
		var flexibility      = $scope.formData.flexibility;
		var fromLocation     = $scope.formData.fromLocation;
		var journeyDate      = $scope.formData.journeyDate;
		var journeyEndDate   = $scope.formData.journeyEndDate;
		var journeyPrice     = $scope.formData.journeyPrice;
		var journeyType      = $scope.formData.journeyType ;
		var routePath        = 'test';
		var seatsAvailable   = $scope.formData.seatsAvailable;
		var toLocation       = $scope.formData.toLocation;
		var luggageSpace     = $scope.formData.luggageSpace;
		var vehicleType      = $scope.formData.vehicleType;
		var flexibleac       = $scope.formData.flexibleac;
		var radioflexible    = $scope.formData.radioflexible;
		var usbflexible      = $scope.formData.usbflexible;
		var slotsflexible    = $scope.formData.slotsflexible;
		var cookingflexible  = $scope.formData.cookingflexible;
		var oksleepflexible  = $scope.formData.oksleepflexible;
		var campingflexible  = $scope.formData.campingflexible;
		var waterflexible    = $scope.formData.waterflexible;
		var total            = '0'; 
        if(parseInt(flexibleac)){
			total =parseInt(total)+parseInt(flexibleac);
		}
		if(parseInt(radioflexible)){
			total = parseInt(total)+parseInt(radioflexible );
		}
		if(parseInt(usbflexible)){
			total = parseInt(total)+parseInt(usbflexible );
		}
		if(parseInt(slotsflexible)){
			total = parseInt(total)+parseInt(slotsflexible );
		}
		if(parseInt(cookingflexible)){
			total = parseInt(total)+parseInt(cookingflexible );
		}
		if(parseInt(oksleepflexible)){
			total = parseInt(total)+parseInt(oksleepflexible );
		}
		if(parseInt(campingflexible)){
			total = parseInt(total)+parseInt(campingflexible );
		}
		if(parseInt(waterflexible)){
			total = parseInt(total)+parseInt(waterflexible );
		}
		var data = 
			{
				 "description": description,
				  "flexibility": flexibility,
				  "fromLocation": fromLocation,
				  "journeyDate": journeyDate,
				  "journeyEndDate":journeyDate,
				  "journeyPrice":journeyPrice,
				  "journeyType": journeyType,
				  "routePath": routePath,
				  "seatsAvailable": seatsAvailable,
				  "toLocation":toLocation,
				  "vehicleOptions": {
					"luggageSpace": luggageSpace,
					"vehicleOptionsMask":total,
					"vehicleType": vehicleType
 				 }

			}

		/**********Post Data*********/
		$http.post('/wikibackend/api/rideShares',{
				data:data,
			}).
		success(function(data,status,headers,config){
		}).
		error(function(data,status,headers,config){
		})
    };
    	
});





app.directive("slider", function() {
    return {
        restrict: 'A',
        scope: {
            config: "=config",
            price: "=model"
        },
        link: function(scope, elem, attrs) {
            var setModel = function(value) {
                scope.model = value;   
            }
            
            $(elem).slider({
                range: false,
	            min: scope.config.min,
	            max: scope.config.max,
                step: scope.config.step,
                slide: function(event, ui) { 
                    scope.$apply(function() {
                        scope.price = ui.value;
                    });
	            }
	        });
    	}
    }
});


// Max Length Directive

app.directive('myMaxlength', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModelCtrl) {
      var maxlength = Number(attrs.myMaxlength);
      function fromUser(text) {
          if (text.length > maxlength) {
            var transformedInput = text.substring(0, maxlength);
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
            return transformedInput;
          } 
          return text;
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  }; 
});



