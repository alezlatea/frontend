app.controller('CheckoutCtrl', ['$scope', '$http',
  function ($scope, $http) {
	
    $http.get('data/checkout.json').success(function(data) {
      $scope.checkout = data;
    });
}]);